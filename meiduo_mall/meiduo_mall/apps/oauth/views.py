from QQLoginTool.QQtool import OAuthQQ
from django.conf import settings
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.settings import api_settings

from oauth.models import OAuthQQUser
from oauth.serializers import QQUserSerializer


class QQURLView(APIView):

    def get(self, request):

        next= request.query_params.get("next")

        if not next:
            next="/"

        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI,
                        state=next
        )
        login_url = oauth.get_qq_url()
        return Response({"login_url":login_url})


class QQUserView(APIView):
    """用户扫码登录的回调处理"""

    def get(self, request):
        # 提取code请求参数
        code = request.query_params.get('code')
        if not code:
            return Response({'message': '缺少code'},
                            status=status.HTTP_400_BAD_REQUEST)
        # 创建工具对象
        oauth = OAuthQQ(client_id=settings.QQ_CLIENT_ID,
                        client_secret=settings.QQ_CLIENT_SECRET,
                        redirect_uri=settings.QQ_REDIRECT_URI)

        try:
            # 使用 code 向QQ服务器请求 access_token
            access_token = oauth.get_access_token(code)
            # 使用 access_token 向QQ服务器请求 openid
            openid = oauth.get_open_id(access_token)
        except:
            return Response({'message': 'QQ服务异常'},
                            status=status.HTTP_503_SERVICE_UNAVAILABLE)

        # 使用openid查询该QQ用户是否在美多商城中绑定过用户
        try:
            oauth_user = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 如果openid没绑定美多商城用户，则需要进行绑定，
            # 在这里将openid返回给前端，绑定时再传递服务器
            return Response({'openid': openid})
        else:
            # 到此表示登录成功，如果openid已绑定美多商城用户，直接生成JWT token，并返回
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            # 获取oauth_user关联的user
            user = oauth_user.user
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            response = Response({
                'token': token,
                'user_id': user.id,
                'username': user.username
            })
            return response


    def post(self, request):

        # 1. 创建序列化器
        s = QQUserSerializer(data=request.data)
        # 2. 校验请求参数是否合法: serializer.is_valid()
        s.is_valid(raise_exception=True)
        # 3. 绑定openid与美多用户:  serializer.save()   -> serializer.create()
        user = s.save()   # 返回绑定的美多用户对象
        # 4. 生成并响应 jwt, user_id, username，完成QQ登录

        # 生成jwt
        from rest_framework_jwt.settings import api_settings
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # 生payload部分的方法(函数)
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER  # 生成jwt的方法(函数)

        payload = jwt_payload_handler(user)  # 生成payload, 得到字典
        token = jwt_encode_handler(payload)  # 生成jwt字符串

        # 响应数据
        context = {
            'token': token,
            'user_id': user.id,
            'username': user.username
        }
        return Response(context)