from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer


def generate_encrypted_openid(openid):
    serializer = TimedJSONWebSignatureSerializer(
        settings.SECRET_KEY, expires_in=600
    )

    return serolizer.dumps({"openid":openid}).decode()