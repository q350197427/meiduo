from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from redis import StrictRedis
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.views import APIView

import logging

from meiduo_mall.libs.yuntongxun.sms import CCP

logger = logging.getLogger('django')


class SmsCodeView(APIView):

    def get(self, request, mobile):

        strict_redis = get_redis_connection("sms_codes")  # type: StrictRedis
        send_flag = strict_redis.get("sms_flag_%s" %mobile)
        if send_flag:
            # return Response({"message":"禁止重复发送短信验证码"}, status=400)
            raise ValidationError({'message': '禁止重复发送短信验证码'})

        import random
        sms_code = "%06d" % random.randint(0, 999999)
        logger.info("获取短信验证码：%s" % sms_code)

        # 云通讯
        # [sms_code, 5]  [短信验证码， 有效期]   1表示id为1的短信测试模板，项目上线换成自己的模板id
        # CCP().send_template_sms(mobile, [sms_code, 5], 1)
        # print(CCP().send_template_sms(mobile, [sms_code, 5], 1))

        # strict_redis.setex("sms_% s" % mobile, 60*5, sms_code)
        # strict_redis.setex("sms_flag_% s" % mobile, 60, 1)
        pipeline = strict_redis.pipeline()
        pipeline.setex("sms_% s" % mobile, 60*5, sms_code)
        pipeline.setex("sms_flag_% s" % mobile, 60, 1)
        result = pipeline.execute()
        print(result)

        return Response({"message": "Ok"})
