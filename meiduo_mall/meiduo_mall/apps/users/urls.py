from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from meiduo_mall.apps.users import views

urlpatterns = [
    url(r"^test/$", views.TestView.as_view()),
    url(r'^test2/$', views.TestView2.as_view()),
    url(r"^users/$",views.CreateUserView.as_view() ),
    url(r'^usernames/(?P<username>\w{5,20})/count/$',
        views.UsernameCountView.as_view()),
    url(r'^authorizations/$', obtain_jwt_token),

]